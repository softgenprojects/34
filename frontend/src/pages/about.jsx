import { VStack, Heading, Text, Button, Flex, Box, Container } from '@chakra-ui/react';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';
import { BrandLogo } from '@components/BrandLogo';

const AboutPage = () => {
  return (
    <Box>
      <NavigationBar />
      <Container maxW='container.xl' p={10}>
        <VStack spacing={8} align='stretch'>
          <BrandLogo />
          <Heading as='h1' size='2xl'>About Marko Kraemer</Heading>
          <Text fontSize='lg'>Hey, my name is Marko! I am 18 years old and I have been running software projects and companies since I was 13. I have scaled up a software development agency to 1+ million euros in revenue, and over the last few years, most of my time has been spent planning and managing custom software development projects.</Text>
          <Text fontSize='lg'>When I first used Generative AI to plan out the complete business logic and data schema in 1.5 hours instead of 8 when creating tasks for my developers, I knew a big change was coming to this industry. This led to the creation of SoftGen.AI, a comprehensive development solution that generates the complete codebase based on project requirements.</Text>
          <Text fontSize='lg'>The rise of GPT and AI is changing the economy - in 10 years it will be fundamentally different than it is today. Latest GPT models allow autonomous AI's to be deployed. Existing processes need to be rethought based on first-principle thinking in all industries; we will be part of that change for custom software development.</Text>
          <Text fontSize='lg'>Whether SoftGen.AI exists or not, autonomous developer AI's are coming; the technology is here, and the execution is important now.</Text>
          <Button colorScheme='blue' size='lg'>Download Resume</Button>
        </VStack>
      </Container>
      <Footer />
    </Box>
  );
};

export default AboutPage;

import { Text, HStack, useBreakpointValue } from '@chakra-ui/react';
import { AiOutlineUser } from 'react-icons/ai';

export const BrandLogo = () => {
  const logoSize = useBreakpointValue({ base: '24px', md: '32px' });
  const fontSize = useBreakpointValue({ base: 'md', md: 'xl' });

  return (
    <HStack spacing={2} alignItems='center'>
      <AiOutlineUser size={logoSize} color='gray.700' />
      <Text fontSize={fontSize} fontWeight='bold' color='gray.700'>
        Marko Kraemer
      </Text>
    </HStack>
  );
};
import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, Link as ChakraLink, Container, SimpleGrid } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';
import Link from 'next/link';
import { useEffect, useState } from 'react';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    // This will set isClient to true only on the client side
    setIsClient(true);
  }, []);

  // Render components that rely on client-side features only after confirming the client side is active
  const renderClientSideContent = () => {
    if (!isClient) return null;

    return (
      <Link href="/about" passHref>
        <ChakraLink as={Button} colorScheme="pink" size="lg" mt={10}>
          Learn More
        </ChakraLink>
      </Link>
    );
  };

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <NavigationBar />
      <Container maxW='container.xl' py={{ base: '4', md: '8' }} px={{ base: '4', lg: '8' }}>
        <VStack spacing="8" align="stretch">
          <Center>
            <BrandLogo />
          </Center>
          <Heading as="h1" size="xl" textAlign="center">
            Welcome to Marko Kraemer's Portfolio
          </Heading>
          <Text textAlign="center">
            Discover the projects and achievements of Marko Kraemer, CEO of SoftGen.ai
          </Text>
          <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
            <Box>
              <Heading as="h3" size="lg">About Marko</Heading>
              <Text mt={4}>
                Hey, my name is Marko! I am 18 years old and I have been running software projects and companies since I was 13. I have scaled up a software development agency to 1+ million euros in revenue.
              </Text>
            </Box>
            <Box>
              <Heading as="h3" size="lg">Achievements</Heading>
              <Text mt={4}>
                Over the last few years, most of my time has been spent planning and managing custom software development projects. My work with Generative AI led to the creation of SoftGen.AI.
              </Text>
            </Box>
          </SimpleGrid>
          {renderClientSideContent()}
        </VStack>
      </Container>
      <Footer />
    </Box>
  );
};

export default Home;

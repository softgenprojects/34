import { Box, Container, Flex, Input, Textarea, Button, Heading, Text, VStack, FormControl, FormLabel, useBreakpointValue } from '@chakra-ui/react';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';
import { FaTwitter, FaLinkedinIn, FaGithub } from 'react-icons/fa';
import { Icon } from '@chakra-ui/icons';

const ContactPage = () => {
  const inputStyles = {
    variant: 'filled',
    _focus: { borderColor: 'gray.300' },
    size: useBreakpointValue({ base: 'md', md: 'lg' })
  };

  return (
    <Box>
      <NavigationBar />
      <Container maxW='container.xl' mt={10}>
        <Flex direction={{ base: 'column', md: 'row' }} justify='space-between' align='center'>
          <Box flex='1' mr={{ base: 0, md: 6 }} mb={{ base: 6, md: 0 }}>
            <Heading mb={4}>Get in Touch</Heading>
            <Text fontSize='lg' mb={8}>Feel free to reach out for collaborations or just a friendly hello 👋</Text>
            <VStack spacing={4} as='form'>
              <FormControl id='name'>
                <FormLabel>Name</FormLabel>
                <Input placeholder='Your Name' {...inputStyles} />
              </FormControl>
              <FormControl id='email'>
                <FormLabel>Email</FormLabel>
                <Input placeholder='Your Email' type='email' {...inputStyles} />
              </FormControl>
              <FormControl id='message'>
                <FormLabel>Message</FormLabel>
                <Textarea placeholder='Your Message' rows={6} {...inputStyles} />
              </FormControl>
              <Button colorScheme='blue' px={10} type='submit'>Send Message</Button>
            </VStack>
          </Box>
          <Box flex='1' ml={{ base: 0, md: 6 }}>
            <Heading mb={4}>Social Links</Heading>
            <Flex direction='column' mt={2}>
              <Icon as={FaTwitter} w={6} h={6} mb={2} />
              <Icon as={FaLinkedinIn} w={6} h={6} mb={2} />
              <Icon as={FaGithub} w={6} h={6} />
            </Flex>
          </Box>
        </Flex>
      </Container>
      <Footer />
    </Box>
  );
};

export default ContactPage;

import React from 'react';
import { FormControl, FormLabel, FormErrorMessage, Input, Textarea, Button } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { ValidateEmail } from '@utilities/ValidateEmail';

export const ContactForm = () => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();

  function onSubmit(values) {
    return new Promise((resolve) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        resolve();
      }, 3000);
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl isInvalid={errors.name}>
        <FormLabel htmlFor='name'>Name</FormLabel>
        <Input
          id='name'
          placeholder='Your Name'
          {...register('name', {
            required: 'This is required',
            minLength: { value: 4, message: 'Minimum length should be 4' },
          })}
        />
        <FormErrorMessage>
          {errors.name && errors.name.message}
        </FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.email} mt={4}>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input
          id='email'
          placeholder='Your Email'
          {...register('email', {
            required: 'This is required',
            validate: ValidateEmail,
          })}
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.message} mt={4}>
        <FormLabel htmlFor='message'>Message</FormLabel>
        <Textarea
          id='message'
          placeholder='Your Message'
          {...register('message', {
            required: 'This is required',
          })}
        />
        <FormErrorMessage>
          {errors.message && errors.message.message}
        </FormErrorMessage>
      </FormControl>

      <Button mt={4} colorScheme='blue' isLoading={isSubmitting} type='submit'>
        Send Message
      </Button>
    </form>
  );
};

import { Box, VStack, Heading, Text, Link } from '@chakra-ui/react';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';

const blogPosts = [
  {
    title: 'The Future of AI in Software Development',
    summary: 'Exploring how AI is revolutionizing the software industry.',
    slug: 'future-ai-software-development'
  },
  {
    title: 'First-Principle Thinking in Tech',
    summary: 'How to approach problem-solving in technology from first principles.',
    slug: 'first-principle-thinking-tech'
  },
  {
    title: 'The Rise of Autonomous Developer AIs',
    summary: 'A look into the future where AIs autonomously build software.',
    slug: 'rise-autonomous-developer-ais'
  }
];

const BlogPostCard = ({ title, summary, slug }) => (
  <Box
    borderWidth='1px'
    borderRadius='lg'
    overflow='hidden'
    p={6}
    my={4}
    bg='white'
    boxShadow='sm'
    _hover={{ boxShadow: 'md' }}
  >
    <Heading size='md' mb={2}>{title}</Heading>
    <Text mb={4}>{summary}</Text>
    <Link color='teal.500' href={`/blog/${slug}`}>Read more</Link>
  </Box>
);

const BlogPage = () => {
  return (
    <Box>
      <NavigationBar />
      <VStack
        as='main'
        spacing={8}
        justifyContent='center'
        alignItems='flex-start'
        m='0 auto 4rem auto'
        maxWidth='700px'
        px={{ base: '5%', md: '10%' }}
        py={8}
      >
        <Heading as='h1' size='2xl' mb={4}>
          Blog
        </Heading>
        {blogPosts.map((post) => (
          <BlogPostCard key={post.slug} {...post} />
        ))}
      </VStack>
      <Footer />
    </Box>
  );
};

export default BlogPage;

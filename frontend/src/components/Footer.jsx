import { Box, Container, Flex, Text, Link, HStack, Icon } from '@chakra-ui/react';
import { FaTwitter, FaLinkedinIn, FaGithub } from 'react-icons/fa';
import { BrandLogo } from '@components/BrandLogo';

export const Footer = () => {
  return (
    <Box as='footer' bg='gray.800' color='white' py={{ base: '4', md: '8' }}>
      <Container maxW='container.xl'>
        <Flex justifyContent='space-between' alignItems='center' flexDirection={{ base: 'column', md: 'row' }}>
          <BrandLogo />
          <HStack spacing={{ base: '2', md: '4' }} mt={{ base: '4', md: '0' }}>
            <Link href='/about' fontSize='sm'>About</Link>
            <Link href='/projects' fontSize='sm'>Projects</Link>
            <Link href='/contact' fontSize='sm'>Contact</Link>
          </HStack>
          <HStack spacing={{ base: '2', md: '4' }} mt={{ base: '4', md: '0' }}>
            <Link href='https://twitter.com' isExternal>
              <Icon as={FaTwitter} w={5} h={5} />
            </Link>
            <Link href='https://linkedin.com' isExternal>
              <Icon as={FaLinkedinIn} w={5} h={5} />
            </Link>
            <Link href='https://github.com' isExternal>
              <Icon as={FaGithub} w={5} h={5} />
            </Link>
          </HStack>
        </Flex>
        <Text textAlign='center' fontSize='sm' mt={{ base: '8', md: '12' }}>
          © {new Date().getFullYear()} Marko Kraemer. All rights reserved.
        </Text>
      </Container>
    </Box>
  );
};
import { HStack, Link, Icon } from '@chakra-ui/react';
import { FaTwitter, FaLinkedinIn, FaGithub } from 'react-icons/fa';

export const SocialLinks = () => {
  return (
    <HStack spacing={4} justify='center'>
      <Link href='https://twitter.com/markokraemer' isExternal>
        <Icon as={FaTwitter} boxSize={{ base: '6', md: '8' }} />
      </Link>
      <Link href='https://linkedin.com/in/markokraemer' isExternal>
        <Icon as={FaLinkedinIn} boxSize={{ base: '6', md: '8' }} />
      </Link>
      <Link href='https://github.com/markokraemer' isExternal>
        <Icon as={FaGithub} boxSize={{ base: '6', md: '8' }} />
      </Link>
    </HStack>
  );
};

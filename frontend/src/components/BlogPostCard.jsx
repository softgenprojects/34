import React from 'react';
import { Box, Heading, Text, Link, Card } from '@chakra-ui/react';

export const BlogPostCard = ({ title, excerpt, publicationDate, link }) => {
  return (
    <Card borderWidth='1px' borderRadius='lg' overflow='hidden' p={4}>
      <Heading fontSize='xl' mb={2}>{title}</Heading>
      <Text mb={4}>{excerpt}</Text>
      <Text fontSize='sm' color='gray.500'>{publicationDate}</Text>
      <Link color='teal.500' href={link} isExternal>
        Read more
      </Link>
    </Card>
  );
};

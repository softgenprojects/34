import { Flex, Box, Link as ChakraLink, useBreakpointValue } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import NextLink from 'next/link';

export const NavigationBar = () => {
  const linkHoverStyle = {
    textDecoration: 'none',
    borderBottom: '2px solid',
    borderColor: 'gray.200'
  };

  const navItemProps = {
    fontSize: useBreakpointValue({ base: 'sm', md: 'md' }),
    fontWeight: 'semibold',
    color: 'gray.600',
    _hover: linkHoverStyle,
    p: 2
  };

  return (
    <Flex
      as='nav'
      align='center'
      justify='space-between'
      wrap='wrap'
      padding={4}
      bg='white'
      color='gray.600'
      boxShadow='sm'
    >
      <Box flexBasis={{ base: '100%', md: 'auto' }}>
        <BrandLogo />
      </Box>
      <Box
        display={{ base: 'none', md: 'block' }}
        flexBasis={{ base: '100%', md: 'auto' }}
      >
        <Flex align='center' justify='flex-end'>
          <NextLink href='/' passHref><ChakraLink as='a' {...navItemProps}>Home</ChakraLink></NextLink>
          <NextLink href='/about' passHref><ChakraLink as='a' {...navItemProps}>About</ChakraLink></NextLink>
          <NextLink href='/projects' passHref><ChakraLink as='a' {...navItemProps}>Projects</ChakraLink></NextLink>
          <NextLink href='/blog' passHref><ChakraLink as='a' {...navItemProps}>Blog</ChakraLink></NextLink>
          <NextLink href='/contact' passHref><ChakraLink as='a' {...navItemProps}>Contact</ChakraLink></NextLink>
        </Flex>
      </Box>
    </Flex>
  );
};
import { Box, Heading, Text, Link, Card } from '@chakra-ui/react';

export const ProjectCard = ({ title, description, technologies, link }) => {
  return (
    <Card p={4} boxShadow='lg' borderRadius='lg' bg='white' color='gray.800'>
      <Heading size='md' mb={2}>{title}</Heading>
      <Text fontSize='sm' mb={4}>{description}</Text>
      <Text fontSize='xs' color='gray.500' mb={4}>Technologies used: {technologies.join(', ')}</Text>
      {link && <Link href={link} color='blue.500' fontWeight='bold' isExternal>View Project</Link>}
    </Card>
  );
};
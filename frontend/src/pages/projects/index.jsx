import { Box, SimpleGrid, Heading, Text, Container } from '@chakra-ui/react';
import { NavigationBar } from '@components/NavigationBar';
import { Footer } from '@components/Footer';

const ProjectCard = ({ title, description }) => (
  <Box borderWidth='1px' borderRadius='lg' overflow='hidden' p={5}>
    <Heading fontSize='xl' mb={3}>{title}</Heading>
    <Text fontSize='md'>{description}</Text>
  </Box>
);

const projectsData = [
  {
    title: 'Software Development Agency',
    description: 'Scaled the agency to 1+ million euros in revenue.'
  },
  {
    title: 'SoftGen.AI',
    description: 'Generative AI for planning and managing custom software development projects.'
  }
];

const ProjectsPage = () => {
  return (
    <Box>
      <NavigationBar />
      <Container maxW='container.xl' mt={10} mb={10}>
        <Heading as='h1' size='2xl' mb={6} textAlign='center'>Projects</Heading>
        <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10}>
          {projectsData.map((project, index) => (
            <ProjectCard key={index} title={project.title} description={project.description} />
          ))}
        </SimpleGrid>
      </Container>
      <Footer />
    </Box>
  );
};

export default ProjectsPage;
